#!/bin/bash

set -xe 

function log() {
  echo -ne "[$(date +'%Y-%m-%dT%H:%M:%S%z')]: $@\n" >&1
}

function err() {
  echo -ne "[$(date +'%Y-%m-%dT%H:%M:%S%z')]: $@\n" >&2
}


#
# Update kernel
#

LATEST_STABLE_VERSION=$(curl -SsfL https://www.kernel.org/releases.json 2>/dev/null| jq '.latest_stable.version' -r )

if [ -z "$LATEST_STABLE_VERSION" ]; then
  err "Cannot extract latest stable linux version"
  exit 1
fi

if apt list --installed 2>/dev/null | grep linux-image | grep $LATEST_STABLE_VERSION; then
  log "Latest stable Linux version $LATEST_STABLE_VERSION is already installed"
	exit 0
fi

log "Installing Linux $LATEST_STABLE_VERSION"

mkdir "/tmp/linux-$LATEST_STABLE_VERSION/" -p

LINKS=$(curl -SsfL https://kernel.ubuntu.com/~kernel-ppa/mainline/v$LATEST_STABLE_VERSION/ | grep -Eow '>linux-.+?(_amd64|_all)\.deb<' | grep -v low | sed "s/<//g" | sed "s/>//g" | sort | uniq)

for LINK in $LINKS; do
  log "Downloading $LINK"
  #curl -SsfL "https://kernel.ubuntu.com/~kernel-ppa/mainline/v$LATEST_STABLE_VERSION/$LINK" -o "/tmp/linux-$LATEST_STABLE_VERSION/$LINK"
  wget -nc "https://kernel.ubuntu.com/~kernel-ppa/mainline/v$LATEST_STABLE_VERSION/$LINK" -O "/tmp/linux-$LATEST_STABLE_VERSION/$LINK" 
done 

# Verify
log "Verify deb packages $(ls /tmp/linux-$LATEST_STABLE_VERSION/*.deb )"

# Add GPG key 
gpg --keyserver hkps://pgp.mit.edu --recv-key "60AA7B6F30434AE68E569963E50C6A0917C622B0"
wget -nc "https://kernel.ubuntu.com/~kernel-ppa/mainline/v$LATEST_STABLE_VERSION/CHECKSUMS" -O "/tmp/linux-$LATEST_STABLE_VERSION/CHECKSUMS" 
wget -nc "https://kernel.ubuntu.com/~kernel-ppa/mainline/v$LATEST_STABLE_VERSION/CHECKSUMS.gpg" -O "/tmp/linux-$LATEST_STABLE_VERSION/CHECKSUMS.gpg" 

## shasum fails causere we download only required files and not all checksummed files
shasum -c CHECKSUMS | grep 'OK' > check_result.log || true

for LINK in $LINKS; do
  log "Validate $LINK"
  grep $LINK check_result.log
done 

log "Installing deb packages $(ls /tmp/linux-$LATEST_STABLE_VERSION/ )"

dpkg --install /tmp/linux-$LATEST_STABLE_VERSION/*.deb 1> /tmp/linux-$LATEST_STABLE_VERSION/installation.log 2> /tmp/linux-$LATEST_STABLE_VERSION/installation.err

log "Checking for missing firmware "

if grep -i "Possible missing firmware" /tmp/linux-$LATEST_STABLE_VERSION/installation.err; then 

  FIRMWARE_REPO_URL="https://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git"

  if USE_REPO; then 
    FIRMWARE_REPO_PATH="/root/Projects/linux/linux-firmware"

    if [ -d /root/Projects/linux/linux-firmware/.git ]; then
      git -C /root/Projects/linux/linux-firmware pull
    else
      git clone $FIRMWARE_REPO_URL /root/Projects/linux/linux-firmware 
    fi

    for MISSING_DRIVER in $( sed -nr "s|W: Possible missing firmware \/lib\/firmware\/([a-z0-9\.\_\/]+?) for module [a-z0-9]*|\1|gp" /tmp/linux-$LATEST_STABLE_VERSION/installation.err ); do
      log "Installing missing driver $MISSING_DRIVER "
      DIR="${MISSING_DRIVER%/*}"

      ln -s /lib/firmware/${MISSING_DRIVER} $FIRMWARE_REPO_PATH/${MISSING_DRIVER}
    done
  else
    for MISSING_DRIVER in $( sed -nr "s|W: Possible missing firmware \/lib\/firmware\/([a-z0-9\.\_\/]+?) for module [a-z0-9]*|\1|gp" /tmp/linux-$LATEST_STABLE_VERSION/installation.err ); do
      log "Copying missing driver ${MISSING_DRIVER} from repo $FIRMWARE_REPO_URL"
      # Prepare folder
      DIR="${MISSING_DRIVER%/*}"
      mkdir /lib/firmware/$DIR -p
      curl -SsfL ${FIRMWARE_REPO_URL}/plain/${MISSING_DRIVER} -O /lib/firmware/${MISSING_DRIVER}
    done
  fi
else
  log "No missing firmware"
fi

exit