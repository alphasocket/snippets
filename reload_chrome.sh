#!/bin/bash
exec <"$0" || exit; read v; read v; exec /usr/bin/osascript - "$@"; exit

-- the above is some shell trickery that lets us write the rest of
-- the file in plain applescript
on run argv
  set targetApp to item 1 of argv
  set delayInSeconds to item 2 of argv
  tell application "Google Chrome"
    activate
    tell application "System Events"
      tell process "Google Chrome"
        delay delayInSeconds
        keystroke "r" using {command down, shift down}
        delay delayInSeconds
      end tell
    end tell
  end tell

  tell application targetApp to activate

end run

